package org.haxe.extension;

import org.haxe.lime.HaxeObject;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.haxe.extension.IntentManagerZ;
import org.haxe.extension.FManagerZ;

public class Get_image_ext extends Extension {
	
	public static HaxeObject callback;
	public static int typeActivity;

	public static String getAppDir() {
		
		FManagerZ fm = new FManagerZ(Extension.mainContext);
		return fm.getDir();

	}


	public static void filesIntent(HaxeObject cb, int tp) {
		
		callback = cb;
		typeActivity = tp;
		
		Handler handler = new Handler(Looper.getMainLooper());
		handler.post(new Runnable() {
		    
		    @Override
		    public void run() {
		    	
		        Extension.mainActivity.startActivityForResult(new Intent(Extension.mainContext,IntentManagerZ.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK),1);

		    }

		});

	}

}
