package ar.com.euda.openfl;

#if flash

import ar.com.euda.openfl.hamburga.UIScreen;
import ar.com.euda.openfl.hamburga.AssetsManager;
import ar.com.euda.openfl.hamburga.ScaledSprite;
import ar.com.euda.openfl.hamburga.Label;
import ar.com.euda.openfl.hamburga.PlainButton;
import ar.com.euda.openfl.hamburga.StringsManager;
import ar.com.euda.openfl.hamburga.Game;
import ar.com.euda.openfl.hamburga.TextUtil;
import ar.com.euda.openfl.hamburga.PlainSprite;
import flash.media.Camera;
import flash.media.Video;
import motion.Actuate;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.StatusEvent;
import flash.system.Security;
import flash.system.SecurityPanel;

class FlashCameraScreen extends UIScreen {
	var background:PlainSprite;
	var callback:Dynamic;
	var video:Video;
	var camera:Camera;

	public function new(_callback:Dynamic) {
		super(0,0);

		callback = _callback;

		//
		//Background
		//
		background = new PlainSprite(720, 1280, 0x000000, 1);
		background.scaleMode = SCALE_XY;
		background.setPositionMode(CENTER_X, CENTER_Y);
		addChild(background);

		var cameraButton:PlainButton = new PlainButton(this, background.originalWidth * 0.46, 100, 0x89CE3D, 0x5f902a, "OK");
		background.addChild(cameraButton);
		cameraButton.setPositionMode(CENTER_X, ALIGN_BOTTOM);
		cameraButton.setMargin(0, 0, 0, 20, false);
		cameraButton.addEventListener(MouseEvent.CLICK, onCameraButton);
	}

	override function appeared(): Void{
		camera = Camera.getCamera();
		if(camera.muted == true){
			Security.showSettings(SecurityPanel.PRIVACY);
			camera.addEventListener(StatusEvent.STATUS, statusHandler);
		} else {
			cameraUnmuted();
		}
	}

	private function statusHandler(event:StatusEvent){
		if(event.code == "Camera.Muted"){
			cameraMuted();
		} else if(event.code == "Camera.Unmuted") {
			cameraUnmuted();
		}
	}

	private function cameraUnmuted (){
		camera.setQuality(0,100); //no bandwidht limit, best image quality

		video =  new Video();
		video.attachCamera(camera);
		addChild(video);
		video.x = cast(ScaledSprite.screenWidth()*0.5 - video.width*0.5, Int);
		video.y = cast(ScaledSprite.screenHeight()*0.5 - video.height*0.5, Int);
	}

	private function cameraMuted (){
		Game.instance.screenManager.popScreen(false, true);
	}

	private function onCameraButton(event:MouseEvent){
		if (camera.muted == true) {
			video.attachCamera(null);
			Game.instance.screenManager.popScreen(false);
			return;
		}

		var bmData:BitmapData = new BitmapData(cast(video.width, Int), cast(video.height, Int));
		bmData.draw(video);

		var bitmap:Bitmap = new Bitmap(bmData);
		var spr:Sprite = new Sprite();
		spr.addChild(bitmap);

		video.attachCamera(null);
		Game.instance.screenManager.popScreen(false);
		Game.instance.screenManager.popScreen(false);

		callback(spr);
	}

	private function onExitButton(event:MouseEvent):Void {
		Game.instance.screenManager.popScreen(false);
	}
}

#end
