package ar.com.euda.openfl;

#if cpp
import cpp.Lib;
#elseif neko
import neko.Lib;
#end

import flash.Lib;
import openfl.display.DisplayObject;
import openfl.events.Event;
import openfl.geom.Matrix;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.utils.ByteArray;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import flash.display.Loader;
import flash.net.URLRequest;
import flash.errors.Error;
import flash.display.BitmapData;
import ar.com.euda.openfl.hamburga.Game;
import ar.com.euda.openfl.hamburga.ScaledSprite;

import openfl.net.URLLoader;
import openfl.net.URLLoaderDataFormat;

#if (flash)
import flash.media.Camera;
#end

#if (android && openfl)
import openfl.utils.JNI;
#end

class Extension_get_image {
	
	public static var instance(get, null):Extension_get_image;
	private static var _instance:Extension_get_image;
	
	private var shouldRotate:Bool = false;
	private var rotation:Float = 0;

	private var cameraOpened:Bool = false;
	private var galeryOpened:Bool = false;

	private var callback:Dynamic;

	#if (openfl && ios)
	private static var extension_get_image_open_camera = Lib.load ("extension_get_image", "extension_get_image_init_camera", 1);
	private static var extension_get_image_open_library = Lib.load ("extension_get_image", "extension_get_image_init_gallery", 1);
	private static var extension_get_image_check_app_directory = Lib.load ("extension_get_image", "extension_get_image_check_app_directory", 0);
	#elseif (openfl && android)
	private static var extension_get_image_files_intent = JNI.createStaticMethod("org/haxe/extension/Get_image_ext", "filesIntent", "(Lorg/haxe/lime/HaxeObject;I)V");
	#end

	public static function get_instance(): Extension_get_image {
		
		if(_instance!=null) return _instance;
		
		_instance = new Extension_get_image();
		return _instance;
		
	}

	
	private function new() {
		
	}

	
	public function canUseCamera():Bool {
		
		#if (flash)
		return (Camera.isSupported == true && Camera.names.length > 0);
		#else
		return true;
		#end
		
	}

	
	public function openCamera(gameInstance:Game, newCallback:Dynamic, ?buttonColor:UInt=0xFF0000, ?buttonShadowColor:UInt=0xFF0000, ?textColor:UInt=0x000000) {
		
		this.callback = newCallback;
		
		#if (ios)
		this.safetyExecutioniOS(extension_get_image_open_camera, this.deviceGalleryFileSelectCallback);
		#elseif (android)
		Lib.postUICallback(function() {
			
			extension_get_image_files_intent(get_instance(), 2);
			
		});
		#elseif (flash)
		var cameraScreen = new FlashCameraScreen(this.callback);
		gameInstance.screenManager.pushScreen(cameraScreen);
		#end
		
		cameraOpened = true;
		galeryOpened = false;
		shouldRotate = false;
		
	}

	
	public function openGalery(newCallback:Dynamic) {
		
		this.callback = newCallback;
		
		#if (ios)
		this.safetyExecutioniOS(extension_get_image_open_library, this.deviceGalleryFileSelectCallback);
		#elseif (android)
		Lib.postUICallback(function() {
			
			extension_get_image_files_intent(get_instance(), 3);
			
		});
		#elseif (flash)
		var fileReference = new flash.net.FileReference();
		
		fileReference.addEventListener(Event.SELECT, function(e:Event) {
			
			e.target.load();
			
		});
		
		fileReference.addEventListener(Event.COMPLETE, function(e:Event) {
			
			var bytes:ByteArray = e.target.data;
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, onPictureLoaded);
			loader.loadBytes (bytes);
			
		});
		
		fileReference.browse();
		#end	
		
		galeryOpened = true;
		cameraOpened = false;
		shouldRotate = false;
		
	}

	public function deviceGalleryFileSelectCallback(i:String):Void {
		
		if (i != null && i != "") {
			
			var elems:Array<String> = i.split(";");
			var path:String = elems[0];
			
			if (path != null && path != "") {
				
				#if android
				rotation = Std.parseFloat(elems[1]);
				
				if (cameraOpened && rotation != 0) {
					
					shouldRotate = true;
					
				}
				#end							
				
				//var loader = new Loader();
				//loader.contentLoaderInfo.addEventListener (Event.COMPLETE, onPictureLoaded);
				//loader.load (new URLRequest (path));

				var loader:URLLoader = new URLLoader();
				loader.dataFormat = URLLoaderDataFormat.BINARY;
				loader.addEventListener(Event.COMPLETE, onPictureLoaded);
				loader.load (new URLRequest (path));
			}
			
		}
		
	}


	private function rotateAroundCenter(obj:DisplayObject, angleDegrees:Float):Void	{
		
		var tm:Matrix = new Matrix();
		var width:Float = obj.width;
		var height:Float = obj.height;
		
		// translate to the new anchor point
		tm.translate(-width * 0.5, -height * 0.5);
		
		// apply rotation to matrix
		tm.rotate(angleDegrees / 180 * Math.PI);

		//solucion cabeza para el giro 90º
		if (angleDegrees == 90 || angleDegrees == 270) {

			width = obj.height;
			height = obj.width;

		}

		tm.translate(width * 0.5, height * 0.5);
		
		// update matrix
		obj.transform.matrix = tm;
		
	}


	private function onPictureLoaded(event:Event) {
		
		#if flash
		var bitmap:Bitmap = cast (event.currentTarget.content);
		#else
		var bmData = BitmapData.loadFromBytes(event.target.data);
		var bitmap:Bitmap = new Bitmap(bmData, openfl.display.PixelSnapping.ALWAYS, true);
		#end

		if (shouldRotate) {

			rotateAroundCenter(bitmap, rotation);
			
		}
		
		//var spr:ScaledSprite = new ScaledSprite();
		//spr.addChild(bitmap);
		
		if (this.callback != null) {
			
			this.callback(bitmap);
			
		}
		
	}


	private function safetyExecutioniOS(fun:Dynamic, callback:Dynamic) {
		
		#if (ios)	
		if (extension_get_image_check_app_directory()) {
			
			fun(callback);
			
		} else {
			
			trace("Can't create tmp directory");
			
		}
		#end
		
	}

}
