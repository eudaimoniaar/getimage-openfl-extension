#!/bin/bash

echo 'Register extension-get-image with haxelib'
haxelib dev extension-get-image `pwd`

if [ $? -eq 0 ]; then
  echo '[OK]'
else
  echo '[ERROR]'
fi

