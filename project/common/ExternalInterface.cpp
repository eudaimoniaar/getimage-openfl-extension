#ifndef IPHONE
#define IMPLEMENT_API
#endif

#include <hx/CFFI.h>
#include "iOSNative.h"
#include <stdio.h>

using namespace extension_get_image;


static value extension_get_image_get_app_dir () 
{
	
	return alloc_string ( getAppDir() );
	
}
DEFINE_PRIM ( extension_get_image_get_app_dir, 0 );

static void extension_get_image_make_callback( char* strdir ){
       //val_call1(*function_callback,alloc_string(strdir));
}
DEFINE_PRIM ( extension_get_image_make_callback, 1 );
static void extension_get_image_init_gallery (value f) 
{
	initAppGallery(f);
	
}
DEFINE_PRIM ( extension_get_image_init_gallery, 1 );

static void extension_get_image_init_camera (value f) 
{
	initAppCamera(f);
	
}
DEFINE_PRIM ( extension_get_image_init_camera, 1 );

static value extension_get_image_check_app_directory () 
{
	return alloc_bool ( checkAppDirectory() );
	
}
DEFINE_PRIM ( extension_get_image_check_app_directory, 0 );


extern "C" void extension_get_image_main() 
{
	
	// Here you could do some initialization, if needed
	
}
DEFINE_ENTRY_POINT( extension_get_image_main );

extern "C" int extension_get_image_register_prims () { return 0; }
