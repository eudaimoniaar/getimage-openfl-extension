package com.euda.testimageextension;

import ar.com.euda.openfl.hamburga.UIScreen;
import ar.com.euda.openfl.hamburga.AssetsManager;
import ar.com.euda.openfl.hamburga.ImageButton;
import ar.com.euda.openfl.hamburga.Label;
import ar.com.euda.openfl.hamburga.PlainButton;
import ar.com.euda.openfl.hamburga.StringsManager;
import ar.com.euda.openfl.hamburga.Game;
import ar.com.euda.openfl.hamburga.TextUtil;
import ar.com.euda.openfl.hamburga.PlainSprite;
import motion.Actuate;
import openfl.events.MouseEvent;
import ar.com.euda.openfl.Extension_get_image;

class SelectPickerScreen extends UIScreen {
	var background:PlainSprite;
	var callback:Dynamic;

	public function new(backgroundColor:UInt=0x000000, buttonColor:UInt=0xFF0000, buttonShadowColor:UInt=0xCC3300, textColor:UInt=0x000000, titleText:String="Elije la fuente de la imagen", cameraButtonText:String="Cámara", galeryButtonText:String="Galería", _callback:Dynamic) {
		super(0,0);

		callback = _callback;

		//
		//Backgrounds
		//
		var transparent:PlainSprite = new PlainSprite(720, 1280, 0x000000, 0.5);
		transparent.scaleMode = SCALE_XY;
		transparent.setPositionMode(CENTER_X, CENTER_Y);
		transparent.addEventListener(MouseEvent.CLICK, onExitButton);
		addChild(transparent);

		background = new PlainSprite(500, 250, backgroundColor, 1);
		background.scaleMode = MAINTAIN_ASPECT;
		background.setPositionMode(CENTER_X, CENTER_Y);
		addChild(background);

		//
		//Title
		//
		var title:Label = TextUtil.createLabel(titleText, 38, 0xFFFFFF, true);
		title.setPositionMode(CENTER_X, ALIGN_TOP);
		title.setMargin(0, 0, 20, 0, false);
		background.addChild(title);

		//
		//Camera Button && Galery Button
		//
		var cameraButton:PlainButton = new PlainButton(this, background.originalWidth * 0.46, 100, buttonColor, buttonShadowColor, cameraButtonText, textColor, 40, true);
		background.addChild(cameraButton);
		cameraButton.setPositionMode(CENTER_X, ALIGN_BOTTOM);
		cameraButton.setMargin(cameraButton.width * 0.52, 0, 0, 20, false);
		cameraButton.addEventListener(MouseEvent.CLICK, onCameraButton);

		var galeryButton:PlainButton = new PlainButton(this, background.originalWidth * 0.46, 100, buttonColor, buttonShadowColor, galeryButtonText, textColor, 40, true);
		background.addChild(galeryButton);
		galeryButton.setPositionMode(CENTER_X, ALIGN_BOTTOM);
		galeryButton.setMargin(0, cameraButton.width * 0.52, 0, 20, false);
		galeryButton.addEventListener(MouseEvent.CLICK, onGaleryButton);

		if (Extension_get_image.get_instance().canUseCamera() == false) {
			cameraButton.enabled = false;
		}
		//
		//Tween Prepare
		//
		this.alpha = 0;		
	}

	private function onCameraButton(event:MouseEvent):Void {
		Extension_get_image.get_instance().openCamera(Game.instance, callback, 0xFF0000, 0xFF0000, 0x000000);
	}

	private function onGaleryButton(event:MouseEvent):Void {
		Extension_get_image.get_instance().openGalery(callback);
	}

	override function appeared(): Void {

		Actuate.tween (this, 0.5, { alpha: 1 });
	}

	private function onExitButton(event:MouseEvent):Void {
		Actuate.tween(this, 0.5, { alpha: 0 }).onComplete(function(){
			
			background.disposeBitmap();
			Game.instance.screenManager.popScreen(false);

		});
	}

	override function onBackButtonPressed():Bool {

		onExitButton(null);

		return false;

	}
}