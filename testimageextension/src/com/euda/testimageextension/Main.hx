package com.euda.testimageextension;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.Assets;
import flash.text.TextField;
import openfl.events.MouseEvent;
import openfl.text.TextFormat;
import openfl.utils.JNI;
import com.euda.testimageextension.SelectPickerScreen;
import ar.com.euda.openfl.hamburga.Game;
import ar.com.euda.openfl.hamburga.Version;

/**
 * ...
 * @author Ipsilon Developments
 */

class Main extends Game 
{
	var inited:Bool;
	//variable that holds the static function that init the desired activity (source chooser, camera or gallery) ANDROID
	public static var filesIntentFunc:Dynamic;
    //variable that holds the static function that returns the app directory ANDROID
	public static var getDirFunc:Dynamic;
	//variable that holds the current instance to pass it to the java function. ANDROID
	public static var instance:Dynamic;
	
	//variable to access ios methods IOS
	public static var iOSGetImgExt:Dynamic;
	
	//just for testing purposes
	private var fileDir:TextField;
	private var orientation:TextField;
	private var appDir:TextField;

	private var btn1:TextField;
	private var btn2:TextField;
	
	/* ENTRY POINT */

	public function new () {
		var font = Assets.getFont("assets/Helvetica Neue.ttf");
		super(720, 1280, font, new Version(1, 0, 0));
		addEventListener(Event.ADDED_TO_STAGE, added);
	}
	
	function resize(e) 
	{
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		if (inited) return;
		inited = true;
		
		appDir = new TextField();
		appDir.y = 60;
		appDir.width = 300;
		addChild(appDir);		

		var format:TextFormat = new TextFormat();
		format.size = 50;
		btn1 = new TextField();
		btn1.y = 120;
		btn1.setTextFormat(format);
		btn1.text = "open picker";
		addChild(btn1);
		btn1.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent){
			Game.instance.screenManager.pushScreen(new SelectPickerScreen(0x000000, 0xFF0000, 0xCC3300, 0x000000, "Elije la fuente de la imagen", "Cámara", "Galería", deviceGalleryFileSelectCallback));
		});

		/*btn2 = new TextField();
		btn2.setTextFormat(format);
		btn2.text = "open camera";
		btn2.y = 180;
		addChild(btn2);
		btn2.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent){
			Extension_get_image.get_instance().openCamera(deviceGalleryFileSelectCallback);
		});*/
		
		// (your code here)
		
		// Stage:
		// stage.stageWidth x stage.stageHeight @ stage.dpiScale
		
		// Assets:
		// nme.Assets.getBitmapData("img/assetname.jpg");
	}
	public function deviceGalleryFileSelectCallback(image:Sprite):Void {
		Game.instance.screenManager.popScreen();
		addChild(image);
	}


	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
	}
}
